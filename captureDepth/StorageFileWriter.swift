//
//  StorageFileWriter.swift
//  captureDepth
//
//  Created by Tim Strauß on 23.03.19.
//  Copyright © 2019 Tim Strauß. All rights reserved.
//

import Foundation
import Photos


/**
 This class is provides image saving functionality to save a 8 or 16 bit image with depth information
 */
class StorageFileWriter {
    
    ///Image counter for 16 bit images
    var imageId16Bit = 0
    ///Image counter for 8 bit images
    var imageId8Bit = 0
    var fileType16Bit = true
    var viewController: ViewController?
    
    
    /**
     Looks up the image files and sets the image counters
     */
    func initializeImageCounter () {
        let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        initFolders(baseDirectory: directoryURL)
        getCounterFor(baseDirectory: directoryURL.appendingPathComponent("16Bit"), bitDepth: 16)
        getCounterFor(baseDirectory: directoryURL.appendingPathComponent("8Bit"), bitDepth: 8)
        viewController!.imageCountLabel.text = "16Bit: " + String(self.imageId16Bit)
        viewController!.imageCountLabel8Bit.text = "8Bit: " + String(self.imageId8Bit)
    }
    
    /**
     Looks up the image folders and creates them if necessary
     - Parameter baseDirectory: Base directory of the image folders
    */
    func initFolders (baseDirectory: URL) {
        let fileURLs = try? FileManager.default.contentsOfDirectory(at: baseDirectory, includingPropertiesForKeys: nil)
        var Bit16 = false
        var Bit8 = false
        for fileURL in fileURLs! {
            if !fileURL.isFileURL {
                if fileURL.lastPathComponent == "16Bit" {
                    Bit16 = true
                } else if fileURL.lastPathComponent == "8Bit" {
                    Bit8 = true
                }
            }
        }
        if !Bit16 {
            try? FileManager.default.createDirectory(at: baseDirectory.appendingPathComponent("16Bit"), withIntermediateDirectories: false, attributes: [:])
        }
        if !Bit8 {
            try? FileManager.default.createDirectory(at: baseDirectory.appendingPathComponent("8Bit"), withIntermediateDirectories: false, attributes: [:])
        }
    }
    
    /**
     Reads the image folder and determines the image counter
     - Parameter baseDirectory: Base directory of the image folders
     - Parameter bitDepth: Pixel format (e.g. 16 for images with 16 bit per component)
    */
    func getCounterFor (baseDirectory: URL, bitDepth: Int) {
        let fileURLs = try? FileManager.default.contentsOfDirectory(at: baseDirectory, includingPropertiesForKeys: nil)
        for fileURL in fileURLs! {
            if fileURL.isFileURL && fileURL.lastPathComponent.count > (14 + (bitDepth / 8)) {
                let fileName = fileURL.lastPathComponent
                guard let imageFileId = Int(fileName[(9 + (bitDepth / 8))..<fileName.count-5]) else {
                    continue
                }
                if bitDepth == 16 && imageFileId > self.imageId16Bit {
                    self.imageId16Bit = imageFileId
                } else if bitDepth == 8 && imageFileId > self.imageId8Bit {
                    self.imageId8Bit = imageFileId
                }
            }
        }
    }
    
    /**
     Removes all images
    */
    func removeAllImages () {
        let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        clearDirectory(directory: directoryURL.appendingPathComponent("16Bit"))
        clearDirectory(directory: directoryURL.appendingPathComponent("8Bit"))
        self.imageId16Bit = 0
        self.imageId8Bit = 0
        initializeImageCounter()
    }
    
    /**
     Removes all images from a directory
     - Parameter directory: Directory to clear
    */
    func clearDirectory (directory: URL) {
        let fileURLs = try? FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil)
        for fileURL in fileURLs! {
            if fileURL.isFileURL {
                do {
                    try FileManager.default.removeItem(at: fileURL)
                } catch {
                    print("Could not remove " + fileURL.absoluteString)
                }
            }
        }
    }
    
    /**
     Writes a color image and a depth image to the filesystem
     - Parameter depthImage: Image of the depth data
     - Parameter colorImage: Original RGB image
    */
    func writeImageFiles (depthImage: CIImage, colorImage: CIImage) {
        if fileType16Bit {
            write16BitImageFiles(depthImage: depthImage, colorImage: colorImage)
        }
        else {
            write8BitImageFiles(depthImage: depthImage, colorImage: colorImage)
        }
    }
    
    /**
     Writes a 8 bit color image and a 16 bit depth image to the filesystem
     - Parameter depthImage: Image of the depth data
     - Parameter colorImage: Original RGB image
     */
    func write16BitImageFiles (depthImage: CIImage, colorImage: CIImage) {
        self.imageId16Bit += 1
        var success = true
        
        var context = CIContext()
        
        let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("16Bit")
        let depthFileName = directoryURL.appendingPathComponent("imgdepth16b" + String(self.imageId16Bit) + ".tiff")
        let colorFileName = directoryURL.appendingPathComponent("imgcolour8b" + String(self.imageId16Bit) + ".tiff")
        
        do {
            try context.writeTIFFRepresentation(of: depthImage, to: depthFileName, format: CIFormat.L16, colorSpace: CGColorSpace(name: CGColorSpace.extendedGray)!, options: [:])
        } catch {
            print("Could not write depth tiff file")
            
        }
        do {
            try context.writeTIFFRepresentation(of: colorImage, to: colorFileName, format: CIFormat.RGBA8, colorSpace: CGColorSpace(name: CGColorSpace.extendedSRGB)!, options: [:])
        } catch {
            print("Could not write color tiff file")
            success = false
        }
        
        if !success {
            self.imageId16Bit -= 1
        }
        viewController!.imageCountLabel.text = "16Bit: " + String(self.imageId16Bit)
    }
    
    /**
     Writes a 8 bit color image and a 8 bit depth image to the filesystem
     - Parameter depthImage: Image of the depth data
     - Parameter colorImage: Original RGB image
     */
    func write8BitImageFiles (depthImage: CIImage, colorImage: CIImage) {
        self.imageId8Bit += 1
        var success = true
        
        var context = CIContext()
        
        let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("8Bit")
        let depthFileName = directoryURL.appendingPathComponent("imgdepth8b" + String(self.imageId8Bit) + ".tiff")
        let colorFileName = directoryURL.appendingPathComponent("imgcolour8b" + String(self.imageId8Bit) + ".tiff")
        
        do {
            try context.writeTIFFRepresentation(of: depthImage, to: depthFileName, format: CIFormat.L8, colorSpace: CGColorSpace(name: CGColorSpace.extendedGray)!, options: [:])
        } catch {
            print("Could not write depth tiff file")
            
        }
        do {
            try context.writeTIFFRepresentation(of: colorImage, to: colorFileName, format: CIFormat.RGBA8, colorSpace: CGColorSpace(name: CGColorSpace.extendedSRGB)!, options: [:])
        } catch {
            print("Could not write color tiff file")
            success = false
        }
        
        if !success {
            self.imageId8Bit -= 1
        }
        viewController!.imageCountLabel8Bit.text = "8Bit: " + String(self.imageId8Bit)
    }
}


extension String {
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
}
