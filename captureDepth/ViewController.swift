//
//  ViewController.swift
//  captureDepth
//
//  Created by Tim Strauß on 18.03.19.
//  Copyright © 2019 Tim Strauß. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    ///View for displaying a capture preview
    @IBOutlet weak var previewView: UIView!
    ///Image-counter for 16 bit images
    @IBOutlet weak var imageCountLabel: UILabel!
    ///Image-counter for 8 bit images
    @IBOutlet weak var imageCountLabel8Bit: UILabel!
    
    var captureSession: AVCaptureSession!
    var imageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    let fileWriter = StorageFileWriter()
    var imageProcessor = ImageProcessor(fileWriter: StorageFileWriter())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fileWriter.viewController = self
        self.fileWriter.initializeImageCounter()
        self.imageProcessor = ImageProcessor(fileWriter: self.fileWriter)
        
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { _ in })
        
        let captureDevice = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .back)!
        try? captureDevice.lockForConfiguration()
        captureDevice.activeFormat = captureDevice.formats[captureDevice.formats.count-1]
        captureDevice.activeDepthDataFormat = captureDevice.activeFormat.supportedDepthDataFormats[1]
        captureDevice.unlockForConfiguration()
        
        do {
            let input = try? AVCaptureDeviceInput(device: captureDevice)
            
            self.imageOutput = AVCapturePhotoOutput()
            
            self.captureSession = AVCaptureSession()
            self.captureSession.beginConfiguration()
            self.captureSession.sessionPreset = .photo
            self.captureSession.addInput(input!)
            
            self.captureSession.addOutput(self.imageOutput)
            self.captureSession.commitConfiguration()
            setUpLivePreview()
            self.imageOutput.isDepthDataDeliveryEnabled = self.imageOutput.isDepthDataDeliverySupported
            self.captureSession.startRunning()
        }
    }

    /**
     Sets up the preview of the camera
    */
    func setUpLivePreview() {
        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        videoPreviewLayer.frame.size = previewView.frame.size
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
    }
    
    @IBAction func takeAPhoto(_ sender: UIButton) {
        setUpLivePreview()
        let photoSettings: AVCapturePhotoSettings
        if self.imageOutput.availablePhotoCodecTypes.contains(.hevc) {
            photoSettings = AVCapturePhotoSettings(format:
                [AVVideoCodecKey: AVVideoCodecType.hevc])
        } else {
            photoSettings = AVCapturePhotoSettings()
        }
        
        photoSettings.flashMode = .auto
        photoSettings.isAutoStillImageStabilizationEnabled =
            self.imageOutput.isStillImageStabilizationSupported
        photoSettings.isHighResolutionPhotoEnabled = self.imageOutput.isHighResolutionCaptureEnabled
        photoSettings.isDepthDataDeliveryEnabled = self.imageOutput.isDepthDataDeliverySupported
        
        imageOutput.capturePhoto(with: photoSettings, delegate: self.imageProcessor)
    }
    
    @IBAction func deleteAllImages(_ sender: UIButton) {
        self.fileWriter.removeAllImages()
    }
    
    @IBAction func BitmodeChanged(_ sender: UISwitch) {
        fileWriter.fileType16Bit = sender.isOn
    }
}

