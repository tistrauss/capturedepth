//
//  ImageProcessor.swift
//  captureDepth
//
//  Created by Tim Strauß on 18.03.19.
//  Copyright © 2019 Tim Strauß. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

/**
 This class is handeling the capture process as well as depth extraction from the image.
 */
class ImageProcessor: NSObject, AVCapturePhotoCaptureDelegate {
    
    ///Provides image saving functionality
    let fileWriter: StorageFileWriter
    
    init (fileWriter: StorageFileWriter) {
        self.fileWriter = fileWriter
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photo: AVCapturePhoto,
                     error: Error?) {
        let photoData = photo.fileDataRepresentation()!
        var depthData = photo.depthData as! AVDepthData
        if depthData.depthDataType != kCVPixelFormatType_DisparityFloat32 {
            depthData = depthData.converting(toDepthDataType: kCVPixelFormatType_DisparityFloat32)
        }
        var depthDataMap = depthData.depthDataMap
        depthDataMap.normalize()
        
        var ciDepthImage = CIImage(cvPixelBuffer: depthDataMap)
        var ciColorImage = CIImage(data: photoData)!
        ciDepthImage = rotateImage(image: ciDepthImage)
        
        fileWriter.writeImageFiles(depthImage: ciDepthImage, colorImage: ciColorImage)
    }
    
    /**
     Rotates an image so that it is displayed correct.
     
     - Parameter image: The image to be rotated
     - Returns: The right rotated image
     
    */
    func rotateImage (image: CIImage) -> CIImage {
        let interfaceTransform = self.interfaceTransform(for: image)
        let transformed = image.transformed(by: interfaceTransform)
        return transformed
    }
    
    /**
     Generates a image transform which can be used to rotate the image in the right position
     
     - Parameter image: The image to be rotated
     - Returns: Image transformation
    */
    func interfaceTransform(for image: CIImage) -> CGAffineTransform {
        var angle: CGFloat = 0
        switch UIApplication.shared.statusBarOrientation {
        case .portrait:
            angle = 3 * .pi / 2
        case .portraitUpsideDown:
            angle = .pi / 2
        case .landscapeLeft:
            angle = .pi
        default:
            angle = 0
        }
        let rotationTransform = CGAffineTransform(rotationAngle: angle)
        
        let size = image.extent.size
        let sizeAfterRotation = size.applying(rotationTransform)
        
        return CGAffineTransform(translationX: -size.width/2, y: -size.height/2)
            .concatenating(rotationTransform)
            .concatenating(CGAffineTransform(translationX: abs(sizeAfterRotation.width/2), y: abs(sizeAfterRotation.height/2)))
    }
}

extension CVPixelBuffer {
    
    /**
     Normalizes the pixel values on values between 1 and 0
     - Remark: Pixel values have to be in 32 bit Float format
    */
    func normalize() {
        
        let width = CVPixelBufferGetWidth(self)
        let height = CVPixelBufferGetHeight(self)
        
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
        let floatBuffer = unsafeBitCast(CVPixelBufferGetBaseAddress(self), to: UnsafeMutablePointer<Float>.self)
        
        var minPixel: Float = 1.0
        var maxPixel: Float = 0.0
        
        for y in 0 ..< height {
            for x in 0 ..< width {
                let pixel = floatBuffer[y * width + x]
                minPixel = min(pixel, minPixel)
                maxPixel = max(pixel, maxPixel)
            }
        }
        
        let range = maxPixel - minPixel
        
        for y in 0 ..< height {
            for x in 0 ..< width {
                let pixel = floatBuffer[y * width + x]
                floatBuffer[y * width + x] = (pixel - minPixel) / range
            }
        }
        
        CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
    }
}
